
from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"


@app.route("/msg_received", methods=['GET', 'POST'])
def sms_ahoy_reply():
    """Respond to incoming messages with a friendly SMS."""
    # Start our response
    resp = MessagingResponse()
    
    #Sender's number
    number = request.form['From']
    # Sender's message
    message_body = request.form['Body']
    
    # Add a message
    if message_body == 'Start':
        resp.message("Thank you for using LaundryAlert! We will message you when your laundry is done.")
    else :
        resp.message("Please try again!")
    return str(resp)


if __name__ == "__main__":
    app.run(debug=True)
